# README #

Windows Holographic Application to view 3D models of people taken from Skanet through Microsoft Hololens as holograms.

### Setup

Import the application to unity. Build it for Windows Platform. Open the <Application>.sln file in Visual Studio and run it on a remote server by specifying the IP address of hololens.